// import { json, urlencoded } from "body-parser";
// import express from "express";
// import mongoose from "mongoose";
// import socket from "socket.io";

const express = require('express');
const mongoose = require('mongoose');
const app = express();
const messageRouter = require('./public/routes/messages');
const loginRouter = require('./public/routes/login');
const db = require('./public/config/database.conf');
const path = require('path');

require('dotenv').config();

const socket = require('socket.io');

const PORT = 8080;

app.use(express.json());
app.use(express.urlencoded({extended:true}))
app.use(express.static("public"));
app.use('/login', loginRouter);

const server = app.listen(PORT);

let join_person = 0;

const io = socket(server);

const uri = process.env.MONGO;
const dbconn = mongoose.connect(uri, {useNewUrlParser: true});
const connection = mongoose.connection;
connection.once('open', () => {
    console.log("Mongo DB established");
})

io.on("connection", function(socket) {
    join_person++;
    console.log(`${join_person} joined`)
    socket.broadcast.emit("joined", `${join_person} people joined`)
    socket.on("disconnect", function() {
        join_person--;
        console.log(`${join_person} joined`);
      });
    socket.on('broadcast', (msg) => {
        socket.broadcast.emit('adminbc', `Administrator: ${msg}`)
    })
    socket.on('ask', (user, msg) => {
        socket.broadcast.emit('received', user, msg);
        // dbconn.then(result => {
        //     let message = new chat({sender: user, message: msg})
        //     message.save();
        // })
    })
    socket.on('clap', (user) => {
        socket.broadcast.emit('getclapped', `${user} clapped`)
        // dbconn.then(result => {
        //     let message = new chat({sender: user, message:':clap'})
        //     message.save();
        // })
    })
})

// set html engine
app.set('views', path.join(__dirname, './public/views/'))
app.set('view engine', 'ejs')

// route html
app.get('/', (req, res) => {
    res.render('index');
})
app.get('/room', (req, res) => {
    res.render('room');
})
app.get('/signup', (req, res) => {
    res.render('signup');
})

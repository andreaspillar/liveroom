### Cara install
`npm install`


### Dependencies
```json
"dotenv": "^10.0.0",
"ejs": "^3.1.6",
"express": "^4.17.1",
"mongodb": "^4.1.2",
"mongoose": "^6.0.6",
"socket.io": "^4.2.0"
```

### Cara run: 
`npm run start`
const router = require("express").Router();
let User = require('../models/User.model');

router.route('/auth').post((req, res) => {
    const tele = req.body.telephone;
    User.findOne({telephone: tele})
        .then(user => {
            if(!user) {
                return res.status(404).json('Anda belum sign up');
            }
            else{
                res.status(200).json({name: user.name, status: 'success'});
            }
        })
})

router.route('/signup').post((req, res) => {
    const telephone = req.body.telephone;
    const name = req.body.name;
    User.findOne({telephone: telephone})
        .then(result => {
            if(result){
                return res.status(400).json("Telephone number already exists");
            }
            else {
                const newUser = new User({
                    telephone, name
                });
                newUser.telephone = telephone;
                newUser.name = name;
                newUser.save().then(
                    user => {
                        return res.status(200).json({status: "success"});
                    }
                )
            }
        })
})

module.exports = router;
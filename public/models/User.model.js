const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    name: {type: String, required: true},
    telephone: {type: String, required: true, unique: true},
}, {
    timestamps: true,
});

const User = mongoose.model('users', userSchema, 'users')
module.exports = User;